/********************************************************************************
** Form generated from reading UI file 'flightswidget.ui'
**
** Created by: Qt User Interface Compiler version 6.0.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FLIGHTSWIDGET_H
#define UI_FLIGHTSWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTableWidget>

QT_BEGIN_NAMESPACE

class Ui_flightswidget
{
public:
    QPushButton *pushButton;
    QTableWidget *tableWidget;

    void setupUi(QDialog *flightswidget)
    {
        if (flightswidget->objectName().isEmpty())
            flightswidget->setObjectName(QString::fromUtf8("flightswidget"));
        flightswidget->resize(632, 300);
        pushButton = new QPushButton(flightswidget);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setGeometry(QRect(550, 270, 75, 23));
        tableWidget = new QTableWidget(flightswidget);
        tableWidget->setObjectName(QString::fromUtf8("tableWidget"));
        tableWidget->setGeometry(QRect(10, 10, 531, 281));

        retranslateUi(flightswidget);

        QMetaObject::connectSlotsByName(flightswidget);
    } // setupUi

    void retranslateUi(QDialog *flightswidget)
    {
        flightswidget->setWindowTitle(QCoreApplication::translate("flightswidget", "Dialog", nullptr));
        pushButton->setText(QCoreApplication::translate("flightswidget", "OK", nullptr));
    } // retranslateUi

};

namespace Ui {
    class flightswidget: public Ui_flightswidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FLIGHTSWIDGET_H
