/********************************************************************************
** Form generated from reading UI file 'adduserwidget.ui'
**
** Created by: Qt User Interface Compiler version 6.0.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ADDUSERWIDGET_H
#define UI_ADDUSERWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTextEdit>

QT_BEGIN_NAMESPACE

class Ui_adduserwidget
{
public:
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    QComboBox *comboBox;
    QLabel *label_6;
    QLabel *label_7;
    QTextEdit *textEdit_5;
    QLabel *label_5;
    QTextEdit *textEdit_4;
    QLabel *label_4;
    QTextEdit *textEdit_2;
    QLabel *label_3;
    QLabel *label_2;
    QLabel *label;
    QTextEdit *textEdit;
    QTextEdit *textEdit_3;

    void setupUi(QDialog *adduserwidget)
    {
        if (adduserwidget->objectName().isEmpty())
            adduserwidget->setObjectName(QString::fromUtf8("adduserwidget"));
        adduserwidget->resize(600, 300);
        pushButton = new QPushButton(adduserwidget);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setGeometry(QRect(10, 260, 75, 23));
        pushButton_2 = new QPushButton(adduserwidget);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));
        pushButton_2->setGeometry(QRect(520, 260, 75, 23));
        comboBox = new QComboBox(adduserwidget);
        comboBox->addItem(QString());
        comboBox->addItem(QString());
        comboBox->addItem(QString());
        comboBox->setObjectName(QString::fromUtf8("comboBox"));
        comboBox->setGeometry(QRect(240, 200, 31, 22));
        label_6 = new QLabel(adduserwidget);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setGeometry(QRect(240, 173, 51, 20));
        label_7 = new QLabel(adduserwidget);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setGeometry(QRect(90, 260, 421, 20));
        textEdit_5 = new QTextEdit(adduserwidget);
        textEdit_5->setObjectName(QString::fromUtf8("textEdit_5"));
        textEdit_5->setGeometry(QRect(298, 120, 242, 31));
        label_5 = new QLabel(adduserwidget);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setGeometry(QRect(298, 97, 101, 20));
        textEdit_4 = new QTextEdit(adduserwidget);
        textEdit_4->setObjectName(QString::fromUtf8("textEdit_4"));
        textEdit_4->setGeometry(QRect(51, 120, 241, 31));
        label_4 = new QLabel(adduserwidget);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(51, 97, 111, 20));
        textEdit_2 = new QTextEdit(adduserwidget);
        textEdit_2->setObjectName(QString::fromUtf8("textEdit_2"));
        textEdit_2->setGeometry(QRect(206, 40, 189, 31));
        label_3 = new QLabel(adduserwidget);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(401, 17, 121, 20));
        label_2 = new QLabel(adduserwidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(206, 17, 121, 20));
        label = new QLabel(adduserwidget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(11, 17, 111, 20));
        textEdit = new QTextEdit(adduserwidget);
        textEdit->setObjectName(QString::fromUtf8("textEdit"));
        textEdit->setGeometry(QRect(11, 40, 189, 31));
        textEdit_3 = new QTextEdit(adduserwidget);
        textEdit_3->setObjectName(QString::fromUtf8("textEdit_3"));
        textEdit_3->setGeometry(QRect(401, 40, 189, 31));

        retranslateUi(adduserwidget);

        QMetaObject::connectSlotsByName(adduserwidget);
    } // setupUi

    void retranslateUi(QDialog *adduserwidget)
    {
        adduserwidget->setWindowTitle(QCoreApplication::translate("adduserwidget", "Dialog", nullptr));
        pushButton->setText(QCoreApplication::translate("adduserwidget", "\320\236\321\202\320\274\320\265\320\275\320\260", nullptr));
        pushButton_2->setText(QCoreApplication::translate("adduserwidget", "OK", nullptr));
        comboBox->setItemText(0, QCoreApplication::translate("adduserwidget", "1", nullptr));
        comboBox->setItemText(1, QCoreApplication::translate("adduserwidget", "2", nullptr));
        comboBox->setItemText(2, QCoreApplication::translate("adduserwidget", "3", nullptr));

        label_6->setText(QCoreApplication::translate("adduserwidget", "\320\240\320\276\320\273\321\214", nullptr));
        label_7->setText(QCoreApplication::translate("adduserwidget", "<html><head/><body><p align=\"center\"/></body></html>", nullptr));
        label_5->setText(QCoreApplication::translate("adduserwidget", "\320\233\320\276\320\263\320\270\320\275", nullptr));
        label_4->setText(QCoreApplication::translate("adduserwidget", "\320\237\320\260\321\200\320\276\320\273\321\214", nullptr));
        label_3->setText(QCoreApplication::translate("adduserwidget", "\320\236\321\202\321\207\320\265\321\201\321\202\320\262\320\276", nullptr));
        label_2->setText(QCoreApplication::translate("adduserwidget", "\320\230\320\274\321\217", nullptr));
        label->setText(QCoreApplication::translate("adduserwidget", "\320\244\320\260\320\274\320\270\320\273\320\270\321\217", nullptr));
    } // retranslateUi

};

namespace Ui {
    class adduserwidget: public Ui_adduserwidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ADDUSERWIDGET_H
