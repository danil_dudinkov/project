#include "adduserwidget.h"
#include "ui_adduserwidget.h"

#include "user.h"

#include <QMessageBox>
#include <QFile>

extern QString myrole;

adduserwidget::adduserwidget(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::adduserwidget)
{
    ui->setupUi(this);
    if (myrole!="1")
        ui->label_7->setText("Вы можете создать только пользователя - пассажира.");
    else
        ui->label_7->setText("");
}

adduserwidget::~adduserwidget()
{
    delete ui;
}

bool exist(QString login)
{
    QFile file("users.txt");
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    QDataStream ist(&file);
    User user("","","","","","");
    while (!ist.atEnd())
    {
        user.load(ist);
        if (login==user.login())
        {
            file.close();
            return true;
        }
    }
    file.close();
    return false;
}

void adduserwidget::on_pushButton_clicked()
{
    if (QMessageBox::question(this, "Exit", "Вы уверены?", QMessageBox::Yes | QMessageBox::No)==QMessageBox::Yes)
        this->close();
}


void adduserwidget::on_pushButton_2_clicked()
{
    QString name, surname, patronymic, login, password, role;
    surname=ui->textEdit->toPlainText().trimmed();
    name=ui->textEdit_2->toPlainText().trimmed();
    patronymic=ui->textEdit_3->toPlainText().trimmed();
    password=ui->textEdit_4->toPlainText().trimmed();
    login=ui->textEdit_5->toPlainText().trimmed();
    if (myrole=="1")
        role=ui->comboBox->currentText();
    else
    {
        role="3";
    }
    if (patronymic=="")
        patronymic="-";
    if(exist(login))
    {
        QMessageBox::warning(this,"title","Этот логин уже занят!");
    }
    else
    {
        QFile outf("users.txt");
        if (!outf.open(QFile::WriteOnly | QFile::Text | QFile::Append))
        {
            QMessageBox::warning(this,"title","file not open");
        }
        outf.open(QIODevice::WriteOnly | QFile::Text | QFile::Append);
        QDataStream ost(&outf);
        if (surname!="" && name!="" && password!="" && login!="")
        {
            User user(name, surname, patronymic, login, password, role);
            if (QMessageBox::question(this, "Exit", "Вы уверены, что хотите добавить пользователя?", QMessageBox::Yes | QMessageBox::No)==QMessageBox::Yes)
            {
                user.save(ost);
                outf.close();
                this->close();
            }
        }
        else
            QMessageBox::warning(this,"Title","Вы не заполнили обязательные поля (все кроме отчество)!");
        outf.close();
    }
}

