#include "deluser.h"
#include "ui_deluser.h"
#include "user.h"
#include "flight.h"

#include <QMessageBox>
#include <QDataStream>
#include <QFile>

extern QVector <flight> flights;
extern QVector <User> users;

deluser::deluser(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::deluser)
{
    ui->setupUi(this);
    QFile inf("users.txt");
    if (!inf.open(QFile::ReadOnly | QFile::Text))
    inf.open(QIODevice::ReadOnly | QFile::Text);
    QDataStream ist(&inf);
    User user;
    while (!ist.atEnd())
    {
        user.load(ist);
        users.append(user);
    }
    inf.close();
    QFile ffile("flights.txt");
    ffile.open(QIODevice::ReadOnly);
    QDataStream fist(&ffile);
    while (!fist.atEnd())
    {
        flight flight;
        flight.load(fist);
        flights.append(flight);
    }
    ffile.close();
    for (int i=0;i<users.size();i++)
    {
        QString itemtext=users[i].name()+" "+users[i].surname()+" "+users[i].patronymic()+" "+users[i].login()+" роль: "+users[i].role();
        ui->comboBox->addItem(itemtext, i);
    }
    if (users.size()==0)
    {
        QMessageBox::warning(this, "Exit", "Пользователей нет!");
        this->close();
    }
}

deluser::~deluser()
{
    flights.clear();
    users.clear();
    delete ui;
}

void deluser::on_pushButton_clicked()
{
    if (QMessageBox::question(this, "Exit", "Вы уверены?", QMessageBox::Yes | QMessageBox::No)==QMessageBox::Yes)
    {
        flights.clear();
        users.clear();
        this->close();
    }
}


void deluser::on_pushButton_2_clicked()
{
    if (QMessageBox::question(this, "Delete", "Вы уверены?", QMessageBox::Yes | QMessageBox::No)==QMessageBox::Yes)
    {
        int i=ui->comboBox->currentIndex();
        QString login=users[i].login();
        for (int j=0;j<flights.size();j++)
        {
            for (int l = flights[j].logins.size()-1;l>=0;l--)
            {
                if (flights[j].logins[l]==login)
                flights[j].logins.remove(l);
            }
        }
        users.remove(i);
        QFile ffile("flights.txt");
        ffile.open(QIODevice::WriteOnly);
        QDataStream ost(&ffile);
        for (int j=0; j<flights.size(); j++)
            flights[j].save(ost);
        ffile.close();
        flights.clear();
        QFile ufile("users.txt");
        ufile.open(QIODevice::WriteOnly);
        QDataStream uost(&ufile);
        for (int j=0; j<users.size(); j++)
            users[j].save(uost);
        ufile.close();
        users.clear();
        this->close();
    }
}

