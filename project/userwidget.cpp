#include "userwidget.h"
#include "flightswidget.h"
#include "ui_userwidget.h"
#include <QMessageBox>

extern bool showmy;

userwidget::userwidget(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::userwidget)
{
    ui->setupUi(this);
}

userwidget::~userwidget()
{
    delete ui;
}

void userwidget::on_pushButton_clicked()
{
    if (QMessageBox::question(this, "Exit", "Вы уверены?", QMessageBox::Yes | QMessageBox::No)==QMessageBox::Yes)
        this->close();
}


void userwidget::on_pushButton_3_clicked()
{
    showmy = false;
    flightswidget window;
    window.setModal(true);
    window.exec();
}

void userwidget::on_pushButton_2_clicked()
{
    showmy = true;
    flightswidget window;
    window.setModal(true);
    window.exec();
}

