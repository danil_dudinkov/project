#ifndef FLIGHT_H
#define FLIGHT_H

#include <QObject>
#include <QVector>

class flight
{
public:
    flight();
    flight(int number, int number_of_seats, QString pointA, QString pointB);
    void load(QDataStream &ist);
    void save(QDataStream &ost);
    QVector <QString> logins;
    int number();
    int number_of_seats();
    QString pointA();
    QString pointB();
    QString flogins();
private:
    int fnumber;
    int fnumber_of_seats;
    QString A;
    QString B;
};

#endif // FLIGHT_H
