#ifndef ADMINWIDGET_H
#define ADMINWIDGET_H

#include <QDialog>

namespace Ui {
class AdminWidget;
}

class AdminWidget : public QDialog
{
    Q_OBJECT

public:
    explicit AdminWidget(QWidget *parent = nullptr);
    ~AdminWidget();

private slots:
    void on_pushButton_7_clicked();

    void on_pushButton_6_clicked();

    void on_pushButton_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_5_clicked();

private:
    Ui::AdminWidget *ui;
};

#endif // ADMINWIDGET_H
