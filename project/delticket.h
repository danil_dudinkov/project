#ifndef DELTICKET_H
#define DELTICKET_H

#include <QDialog>

namespace Ui {
class delticket;
}

class delticket : public QDialog
{
    Q_OBJECT

public:
    explicit delticket(QWidget *parent = nullptr);
    ~delticket();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::delticket *ui;
};

#endif // DELTICKET_H
