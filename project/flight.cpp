#include "flight.h"

#include <QFile>

flight::flight()
{

}

flight::flight(int number, int number_of_seats, QString pointA, QString pointB)
    :fnumber(number),
    fnumber_of_seats(number_of_seats),
    A(pointA),
    B(pointB)
{

}

void flight::save(QDataStream &ost)
{
    QString n = QString::number(logins.size());
    ost << QString::number(fnumber) << A << B << QString::number(fnumber_of_seats) << n;
    for (int i=0;i<logins.size();i++)
        ost<<logins[i];
}

void flight::load(QDataStream &ist)
{
    QString c;
    int n;
    ist >> c;
    fnumber = c.toInt();
    ist >> A;
    ist >> B;
    ist >> c;
    fnumber_of_seats = c.toInt();
    ist >> c;
    n=c.toInt();
    for (int i=0; i<n; i++)
    {
        if (ist.atEnd())
            break;
        ist>>c;
        logins.append(c);
    }
}

int flight::number()
{
    return (fnumber);
}

int flight::number_of_seats()
{
    return (fnumber_of_seats);
}

QString flight::pointA()
{
    return (A);
}

QString flight::pointB()
{
    return (B);
}

QString flight::flogins()
{
    QString a="";
    for (int i=0; i< logins.size(); i++)
        a+=logins[i]+" ";
    return a;
}
