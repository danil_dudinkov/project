#ifndef USER_H
#define USER_H

#include <QObject>

#include <QDataStream>
#include <QString>

class User
{
public:
    User();
    User(QString name, QString surname, QString patronymic, QString login, QString password, QString role);
    void load(QDataStream &ist);
    void save(QDataStream &ost);
    QString name();
    QString surname();
    QString patronymic();
    QString login();
    QString password();
    QString role();
private:
    QString uname;
    QString usurname;
    QString upatronymic;
    QString ulogin;
    QString upassword;
    QString urole;
};

#endif // USER_H
