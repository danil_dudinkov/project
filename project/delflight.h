#ifndef DELFLIGHT_H
#define DELFLIGHT_H

#include <QDialog>

namespace Ui {
class delflight;
}

class delflight : public QDialog
{
    Q_OBJECT

public:
    explicit delflight(QWidget *parent = nullptr);
    ~delflight();

private slots:
    void on_pushButton_2_clicked();

    void on_pushButton_clicked();

private:
    Ui::delflight *ui;
};

#endif // DELFLIGHT_H
