#include "delflight.h"
#include "ui_delflight.h"
#include "flight.h"

#include <QMessageBox>
#include <QDataStream>
#include <QFile>

extern QVector <flight> flights;


delflight::delflight(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::delflight)
{
    ui->setupUi(this);
    QFile ffile("flights.txt");
    ffile.open(QIODevice::ReadOnly);
    QDataStream fist(&ffile);
    while (!fist.atEnd())
    {
        flight flight;
        flight.load(fist);
        flights.append(flight);
    }
    ffile.close();
    for (int i=0;i<flights.size();i++)
    {
        QString itemtext="№"+QString::number(flights[i].number())+" "+flights[i].pointA()+" - "+flights[i].pointB();
        ui->comboBox->addItem(itemtext, i);
    }
    if (flights.size()==0)
    {
        QMessageBox::warning(this, "Exit", "Рейсов нет!");
        this->close();
    }
}

delflight::~delflight()
{
    flights.clear();
    delete ui;
}

void delflight::on_pushButton_2_clicked()
{
    if (QMessageBox::question(this, "Exit", "Вы уверены?", QMessageBox::Yes | QMessageBox::No)==QMessageBox::Yes)
    {
        flights.clear();
        this->close();
    }
}



void delflight::on_pushButton_clicked()
{
    if (QMessageBox::question(this, "Delete", "Вы уверены?", QMessageBox::Yes | QMessageBox::No)==QMessageBox::Yes)
    {
        int i=ui->comboBox->currentIndex();
        QFile ffile("flights.txt");
        ffile.open(QIODevice::WriteOnly);
        QDataStream ost(&ffile);
        for (int j=0; j<flights.size(); j++)
            if (j!=i)
                flights[j].save(ost);
        ffile.close();
        flights.clear();
        this->close();
    }
}

