#ifndef USERWIDGET_H
#define USERWIDGET_H

#include <QDialog>

namespace Ui {
class userwidget;
}

class userwidget : public QDialog
{
    Q_OBJECT

public:
    explicit userwidget(QWidget *parent = nullptr);
    ~userwidget();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::userwidget *ui;
};

#endif // USERWIDGET_H
