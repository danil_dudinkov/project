#include "adminwidget.h"
#include "flightswidget.h"
#include "ui_adminwidget.h"
#include "adduserwidget.h"
#include "addflightwidget.h"
#include "deluser.h"
#include "delflight.h"
#include "delticket.h"

#include <QMessageBox>

extern bool showmy;

AdminWidget::AdminWidget(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AdminWidget)
{
    ui->setupUi(this);
}

AdminWidget::~AdminWidget()
{
    delete ui;
}

void AdminWidget::on_pushButton_7_clicked()
{
    if (QMessageBox::question(this, "Exit", "Вы уверены?", QMessageBox::Yes | QMessageBox::No)==QMessageBox::Yes)
        this->close();
}


void AdminWidget::on_pushButton_6_clicked()
{
    showmy = false;
    flightswidget window;
    window.setModal(true);
    window.exec();
}


void AdminWidget::on_pushButton_clicked()
{
    adduserwidget window;
    window.setModal(true);
    window.exec();
}


void AdminWidget::on_pushButton_3_clicked()
{
    addflightwidget window;
    window.setModal(true);
    window.exec();
}


void AdminWidget::on_pushButton_2_clicked()
{
    deluser window;
    window.setModal(true);
    window.exec();
}


void AdminWidget::on_pushButton_4_clicked()
{
    delflight window;
    window.setModal(true);
    window.exec();
}


void AdminWidget::on_pushButton_5_clicked()
{
    delticket window;
    window.setModal(true);
    window.exec();
}
