#ifndef FLIGHTSWIDGET_H
#define FLIGHTSWIDGET_H

#include <QDialog>
#include <QStandardItemModel>

namespace Ui {
class flightswidget;
}

class flightswidget : public QDialog
{
    Q_OBJECT

public:
    explicit flightswidget(QWidget *parent = nullptr);
    ~flightswidget();

private slots:
    void on_pushButton_clicked();

private:
    Ui::flightswidget *ui;
    QStandardItemModel *model;
};

#endif // FLIGHTSWIDGET_H
