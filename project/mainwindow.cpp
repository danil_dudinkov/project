#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "adminwidget.h"
#include "cashierwidget.h"
#include "flightswidget.h"
#include "userwidget.h"

#include "user.h"

#include <QMessageBox>
#include <QDataStream>
#include <QFile>

QString myrole;
QString mylogin;

bool showmy;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QFile inf("users.txt");
    if (!inf.open(QFile::ReadOnly | QFile::Text))
    {
        inf.open(QIODevice::WriteOnly);
        User user("user","user","user","user","user","3");
        User admin("admin","admin","admin","admin","admin","1");
        User cashier("cashier","cashier","cashier","cashier","cashier","2");
        QDataStream ist(&inf);
        admin.save(ist);
        cashier.save(ist);
        user.save(ist);
        inf.close();
    }
    else
        inf.close();
    QFile flights("flights.txt");
    if (!flights.open(QFile::ReadOnly | QFile::Text))
    {
        flights.open(QIODevice::WriteOnly);
        QDataStream ist(&flights);
        flights.close();
    }
    else
        flights.close();
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_pushButton_3_clicked()
{
    if (QMessageBox::question(this, "Exit", "Вы уверены?", QMessageBox::Yes | QMessageBox::No)==QMessageBox::Yes)
        this->close();
}


void MainWindow::on_pushButton_clicked()
{
    User user;
    QFile inf("users.txt");
    if (!inf.open(QFile::ReadOnly | QFile::Text))
    {
        QMessageBox::warning(this,"title","file not open");
    }
    inf.open(QIODevice::ReadOnly | QFile::Text);
    QDataStream ist(&inf);
    QString login, password;
    login=ui->textEdit->toPlainText().trimmed();
    password=ui->textEdit_2->toPlainText().trimmed();
    while (!ist.atEnd())
    {
        user.load(ist);
        if (login==user.login())
        {
            if (password==user.password())
            {
                myrole=user.role();
                mylogin=user.login();
                if (user.role()=="1")
                {
                    AdminWidget window;
                    window.setModal(true);
                    window.exec();
                    break;
                }
                else if (user.role()=="2")
                {
                    cashierwidget window;
                    window.setModal(true);
                    window.exec();
                    break;
                }
                else
                {
                    userwidget window;
                    window.setModal(true);
                    window.exec();
                    break;
                }
            }
            else
            {
                QMessageBox::warning(this,"title","Неправильный пароль!");
                break;
            }
        }
    }
    inf.close();
}


void MainWindow::on_pushButton_2_clicked()
{
    showmy = false;
    myrole="4";
    flightswidget window;
    window.setModal(true);
    window.exec();
}

