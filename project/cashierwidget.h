#ifndef CASHIERWIDGET_H
#define CASHIERWIDGET_H

#include <QDialog>

namespace Ui {
class cashierwidget;
}

class cashierwidget : public QDialog
{
    Q_OBJECT

public:
    explicit cashierwidget(QWidget *parent = nullptr);
    ~cashierwidget();

private slots:
    void on_pushButton_7_clicked();

    void on_pushButton_6_clicked();

    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_5_clicked();

private:
    Ui::cashierwidget *ui;
};

#endif // CASHIERWIDGET_H
