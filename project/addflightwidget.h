#ifndef ADDFLIGHTWIDGET_H
#define ADDFLIGHTWIDGET_H

#include <QDialog>

namespace Ui {
class addflightwidget;
}

class addflightwidget : public QDialog
{
    Q_OBJECT

public:
    explicit addflightwidget(QWidget *parent = nullptr);
    ~addflightwidget();

private slots:
    void on_pushButton_2_clicked();

    void on_pushButton_clicked();

private:
    Ui::addflightwidget *ui;
};

#endif // ADDFLIGHTWIDGET_H
