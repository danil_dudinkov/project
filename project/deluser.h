#ifndef DELUSER_H
#define DELUSER_H

#include <QDialog>

namespace Ui {
class deluser;
}

class deluser : public QDialog
{
    Q_OBJECT

public:
    explicit deluser(QWidget *parent = nullptr);
    ~deluser();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::deluser *ui;
};

#endif // DELUSER_H
