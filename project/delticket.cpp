#include "delticket.h"
#include "ui_delticket.h"
#include "user.h"
#include "flight.h"

#include <QMessageBox>
#include <QDataStream>
#include <QFile>

extern QVector <User> users;
extern QVector <flight> flights;

int finduser(QString login)
{
    for (int i=0;i<users.size();i++)
        if (login==users[i].login())
            return i;
    return -1;
}

delticket::delticket(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::delticket)
{
    ui->setupUi(this);
    QFile inf("users.txt");
    if (!inf.open(QFile::ReadOnly | QFile::Text))
    inf.open(QIODevice::ReadOnly | QFile::Text);
    QDataStream ist(&inf);
    User user;
    while (!ist.atEnd())
    {
        user.load(ist);
        users.append(user);
    }
    inf.close();
    QFile ffile("flights.txt");
    ffile.open(QIODevice::ReadOnly);
    QDataStream fist(&ffile);
    while (!fist.atEnd())
    {
        flight flight;
        flight.load(fist);
        flights.append(flight);
    }
    ffile.close();
    bool needexit = true;
    for (int i=0;i<flights.size();i++)
    {
        for (int j=0;j<flights[i].logins.size();j++)
        {
            QString itemtext="№"+QString::number(flights[i].number())+" "+flights[i].pointA()+" - "+flights[i].pointB();
            itemtext=itemtext+": "+flights[i].logins[j];
            int num=finduser(flights[i].logins[j]);
            if (num!=-1)
                itemtext=itemtext+" "+users[num].name()+" "+users[num].surname()+" "+users[num].patronymic();
            ui->comboBox->addItem(itemtext, i+j);
            needexit = false;
        }
    }
    if (flights.size()==0)
    {
        QMessageBox::warning(this, "Exit", "Рейсов нет!");
        this->close();
    }
    if (needexit)
    {
        QMessageBox::warning(this, "Exit", "Билетов нет!");
        this->close();
    }
}

delticket::~delticket()
{
    flights.clear();
    users.clear();
    delete ui;
}

void delticket::on_pushButton_clicked()
{
    if (QMessageBox::question(this, "Exit", "Вы уверены?", QMessageBox::Yes | QMessageBox::No)==QMessageBox::Yes)
    {
        flights.clear();
        users.clear();
        this->close();
    }
}


void delticket::on_pushButton_2_clicked()
{
    if (QMessageBox::question(this, "Delete", "Вы уверены?", QMessageBox::Yes | QMessageBox::No)==QMessageBox::Yes)
    {
        int i=ui->comboBox->currentIndex();
        QFile ffile("flights.txt");
        ffile.open(QIODevice::WriteOnly);
        QDataStream ost(&ffile);
        for (int j=0; j<flights.size(); j++)
        {
            for (int l=0; l<flights[j].logins.size();l++)
            {
                if (i==0)
                    flights[j].logins.remove(l);
                i--;
            }
            flights[j].save(ost);
        }
        ffile.close();
        flights.clear();
        users.clear();
        this->close();
    }
}
