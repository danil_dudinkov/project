#ifndef TICKETSSALE_H
#define TICKETSSALE_H

#include <QDialog>

namespace Ui {
class TicketsSale;
}

class TicketsSale : public QDialog
{
    Q_OBJECT

public:
    explicit TicketsSale(QWidget *parent = nullptr);
    ~TicketsSale();

private slots:
    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_clicked();

private:
    Ui::TicketsSale *ui;
};

#endif // TICKETSSALE_H
