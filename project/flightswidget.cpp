#include "flightswidget.h"
#include "ui_flightswidget.h"
#include "flight.h"

#include <QMessageBox>
#include <QDataStream>
#include <QFile>

extern bool showmy;
extern QString myrole;
extern QString mylogin;

extern QVector <flight> flights;

bool exist(flight flight)
{
    for (int i=0; i<flight.logins.size();i++)
        if (mylogin==flight.logins[i])
            return true;
    return false;
}

int countmy(int j)
{
    int s=0;
    for (int i=0; i<flights[j].logins.size(); i++)
    {
        if (flights[j].logins[i]==mylogin)
            s++;
    }
    return s;
}

flightswidget::flightswidget(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::flightswidget)
{
    ui->setupUi(this);
    QFile file("flights.txt");
    file.open(QIODevice::ReadOnly);
    QDataStream ist(&file);
    while (!ist.atEnd())
    {
        flight flight;
        flight.load(ist);
        if (showmy)
        {
            if (exist(flight))
            {
                flights.append(flight);
            }
        }
        else
            flights.append(flight);
    }
    file.close();
    ui->tableWidget->setRowCount(flights.size()+1);
    if (myrole=="1" || myrole=="3")
        ui->tableWidget->setColumnCount(6);
    else
        ui->tableWidget->setColumnCount(5);
    QTableWidgetItem *title1 = new QTableWidgetItem(tr("Номер рейса"));
    QTableWidgetItem *title2 = new QTableWidgetItem(tr("Число мест"));
    QTableWidgetItem *title3 = new QTableWidgetItem(tr("Пункт отправления"));
    QTableWidgetItem *title4 = new QTableWidgetItem(tr("Пункт назначения"));
    QTableWidgetItem *title5 = new QTableWidgetItem(tr("Число свободных мест"));
    if (myrole=="3")
    {
        QTableWidgetItem *title6 = new QTableWidgetItem(tr("Количество моих билетов"));
            ui->tableWidget->setItem(0, 5, title6);
    }
    ui->tableWidget->setItem(0, 0, title1);
    ui->tableWidget->setItem(0, 1, title2);
    ui->tableWidget->setItem(0, 2, title3);
    ui->tableWidget->setItem(0, 3, title4);
    ui->tableWidget->setItem(0, 4, title5);
    for (int i=1; i<flights.size()+1; i++)
    {
        QTableWidgetItem *a = new QTableWidgetItem(tr("%1").arg(flights[i-1].number()));
        ui->tableWidget->setItem(i, 0, a);
        QTableWidgetItem *b = new QTableWidgetItem(tr("%1").arg(flights[i-1].number_of_seats()));
        ui->tableWidget->setItem(i, 1, b);
        QTableWidgetItem *c = new QTableWidgetItem(tr("%1").arg(flights[i-1].pointA()));
        ui->tableWidget->setItem(i, 2, c);
        QTableWidgetItem *d = new QTableWidgetItem(tr("%1").arg(flights[i-1].pointB()));
        ui->tableWidget->setItem(i, 3, d);
        QTableWidgetItem *f = new QTableWidgetItem(tr("%1").arg(flights[i-1].number_of_seats()-flights[i-1].logins.size()));
        ui->tableWidget->setItem(i, 4, f);
        if (myrole=="1")
        {
            QTableWidgetItem *e = new QTableWidgetItem(tr("%1").arg(flights[i-1].flogins()));
            ui->tableWidget->setItem(i, 5, e);
        }
        if (myrole=="3")
        {
            QTableWidgetItem *e = new QTableWidgetItem(tr("%1").arg(countmy(i-1)));
            ui->tableWidget->setItem(i, 5, e);
        }
    }
}

flightswidget::~flightswidget()
{
    flights.clear();
    delete ui;
}

void flightswidget::on_pushButton_clicked()
{
    flights.clear();
    this->close();
}
