#include "cashierwidget.h"
#include "flightswidget.h"
#include "ui_cashierwidget.h"
#include "adduserwidget.h"
#include "ticketssale.h"
#include "deluser.h"
#include "delticket.h"

#include <QMessageBox>

extern bool showmy;

cashierwidget::cashierwidget(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::cashierwidget)
{
    ui->setupUi(this);
}

cashierwidget::~cashierwidget()
{
    delete ui;
}

void cashierwidget::on_pushButton_7_clicked()
{
    if (QMessageBox::question(this, "Exit", "Вы уверены?", QMessageBox::Yes | QMessageBox::No)==QMessageBox::Yes)
        this->close();
}


void cashierwidget::on_pushButton_6_clicked()
{
    showmy = false;
    flightswidget window;
    window.setModal(true);
    window.exec();
}


void cashierwidget::on_pushButton_clicked()
{
    adduserwidget window;
    window.setModal(true);
    window.exec();
}


void cashierwidget::on_pushButton_2_clicked()
{
    TicketsSale window;
    window.setModal(true);
    window.exec();
}


void cashierwidget::on_pushButton_4_clicked()
{
    deluser window;
    window.setModal(true);
    window.exec();
}


void cashierwidget::on_pushButton_5_clicked()
{
    delticket window;
    window.setModal(true);
    window.exec();
}
