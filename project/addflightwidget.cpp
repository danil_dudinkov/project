#include "addflightwidget.h"
#include "ui_addflightwidget.h"
#include "flight.h"

#include <QMessageBox>
#include <QDataStream>
#include <QFile>

addflightwidget::addflightwidget(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::addflightwidget)
{
    ui->setupUi(this);
}

addflightwidget::~addflightwidget()
{
    delete ui;
}

bool exist(int num)
{
    QFile file("flights.txt");
    file.open(QIODevice::ReadOnly);
    QDataStream ist(&file);
    flight flight(0, 0, "", "");
    while (!ist.atEnd())
    {
        flight.load(ist);
        if (num==flight.number())
        {
            file.close();
            return true;
        }
    }
    file.close();
    return false;
}

void addflightwidget::on_pushButton_2_clicked()
{
    if (QMessageBox::question(this, "Exit", "Вы уверены?", QMessageBox::Yes | QMessageBox::No)==QMessageBox::Yes)
        this->close();
}


void addflightwidget::on_pushButton_clicked()
{
    int num, n;
    QString A, B;
    num = ui->spinBox->value();
    n = ui->spinBox_2->value();
    A = ui->textEdit->toPlainText().trimmed();
    B = ui->textEdit_2->toPlainText().trimmed();
    if (exist(num))
    {
        QMessageBox::warning(this,"title","Номер рейса уже занят!");
    }
    else
    {
        {
            QFile outf("flights.txt");
            if (!outf.open(QFile::WriteOnly | QFile::Append))
            {
                QMessageBox::warning(this,"title","file not open");
            }
            outf.open(QIODevice::WriteOnly | QFile::Append);
            QDataStream ost(&outf);
            if (n!=0 && A!="" && B!="")
            {
                flight flight(num, n, A, B);
                if (QMessageBox::question(this, "Exit", "Вы уверены, что хотите добавить рейс?", QMessageBox::Yes | QMessageBox::No)==QMessageBox::Yes)
                {
                    flight.save(ost);
                    outf.close();
                    this->close();
                }
            }
            else
                QMessageBox::warning(this,"title","Заполнены не все поля!");
            outf.close();
        }
    }
}

