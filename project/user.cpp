#include "user.h"

User::User()
{
}

User::User(QString name, QString surname, QString patronymic, QString login, QString password, QString role)
    :uname(name),
    usurname(surname),
    upatronymic(patronymic),
    ulogin(login),
    upassword(password),
    urole(role)
{
}

void User::save(QDataStream &ost)
{
    ost << uname << usurname << upatronymic << ulogin << upassword << urole;
}

void User::load(QDataStream &ist)
{
    ist >> uname >> usurname >> upatronymic >> ulogin >> upassword  >> urole;
}

QString User::name()
{
    return(uname);
}

QString User::surname()
{
    return(usurname);
}

QString User::patronymic()
{
    return(upatronymic);
}

QString User::login()
{
    return(ulogin);
}

QString User::password()
{
    return(upassword);
}

QString User::role()
{
    return(urole);
}
