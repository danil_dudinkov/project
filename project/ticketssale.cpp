#include "ticketssale.h"
#include "ui_ticketssale.h"
#include "user.h"
#include "flight.h"

#include <QMessageBox>
#include <QDataStream>
#include <QFile>

QVector <flight> flights;
QVector <User> users;

void LoadFlights()
{
    QFile ffile("flights.txt");
    ffile.open(QIODevice::ReadOnly);
    QDataStream ist(&ffile);
    while (!ist.atEnd())
    {
        flight flight;
        flight.load(ist);
        flights.append(flight);
    }
    ffile.close();
}

void LoadUsers()
{
    QFile inf("users.txt");
    if (!inf.open(QFile::ReadOnly | QFile::Text))
    inf.open(QIODevice::ReadOnly | QFile::Text);
    QDataStream ist(&inf);
    User user;
    while (!ist.atEnd())
    {
        user.load(ist);
        if (user.role()=="3")
            users.append(user);
    }
    inf.close();
}

TicketsSale::TicketsSale(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TicketsSale)
{
    ui->setupUi(this);
    LoadFlights();
    LoadUsers();
    for (int i=0;i<flights.size();i++)
    {
        QString itemtext="Рейс №"+QString::number(flights[i].number())+": "+flights[i].pointA()+" - "+flights[i].pointB();
        ui->comboBox->addItem(itemtext, i);
    }
    for (int i=0;i<users.size();i++)
    {
        QString itemtext=users[i].name()+" "+users[i].surname()+" "+users[i].patronymic()+" "+users[i].login();
        ui->comboBox_2->addItem(itemtext, i);
    }
    if (users.size()==0)
    {
        QMessageBox::warning(this, "Exit", "Пользователей нет!");
        this->close();
    }
    if (flights.size()==0)
    {
        QMessageBox::warning(this, "Exit", "Рейсов нет!");
        this->close();
    }
}

TicketsSale::~TicketsSale()
{
    flights.clear();
    users.clear();
    delete ui;
}

void TicketsSale::on_pushButton_2_clicked()
{
    if (QMessageBox::question(this, "Exit", "Вы уверены?", QMessageBox::Yes | QMessageBox::No)==QMessageBox::Yes)
    {
        flights.clear();
        users.clear();
        this->close();
    }
}

void TicketsSale::on_pushButton_3_clicked()
{
    int i, j;
    i = ui->comboBox->currentIndex();
    j = ui->comboBox_2->currentIndex();
    if (flights[i].number_of_seats()-flights[i].logins.size()>0)
        flights[i].logins.append(users[j].login());
    else
        QMessageBox::warning(this, "Warning", "Свободных мест больше нет!");
}

void TicketsSale::on_pushButton_clicked()
{
    if (QMessageBox::question(this, "Exit", "Вы уверены?", QMessageBox::Yes | QMessageBox::No)==QMessageBox::Yes)
    {
        QFile ffile("flights.txt");
        ffile.open(QIODevice::WriteOnly);
        QDataStream ost(&ffile);
        for (int i=0; i<flights.size(); i++)
            flights[i].save(ost);
        ffile.close();
        flights.clear();
        users.clear();
        this->close();
    }
}
