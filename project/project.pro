QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    addflightwidget.cpp \
    adduserwidget.cpp \
    adminwidget.cpp \
    cashierwidget.cpp \
    delflight.cpp \
    delticket.cpp \
    deluser.cpp \
    flight.cpp \
    flightswidget.cpp \
    main.cpp \
    mainwindow.cpp \
    ticketssale.cpp \
    user.cpp \
    userwidget.cpp

HEADERS += \
    addflightwidget.h \
    adduserwidget.h \
    adminwidget.h \
    cashierwidget.h \
    delflight.h \
    delticket.h \
    deluser.h \
    flight.h \
    flightswidget.h \
    mainwindow.h \
    ticketssale.h \
    user.h \
    userwidget.h

FORMS += \
    addflightwidget.ui \
    adduserwidget.ui \
    adminwidget.ui \
    cashierwidget.ui \
    delflight.ui \
    delticket.ui \
    deluser.ui \
    flightswidget.ui \
    mainwindow.ui \
    ticketssale.ui \
    userwidget.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
