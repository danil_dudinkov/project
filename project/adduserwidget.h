#ifndef ADDUSERWIDGET_H
#define ADDUSERWIDGET_H

#include <QDialog>

namespace Ui {
class adduserwidget;
}

class adduserwidget : public QDialog
{
    Q_OBJECT

public:
    explicit adduserwidget(QWidget *parent = nullptr);
    ~adduserwidget();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::adduserwidget *ui;
};

#endif // ADDUSERWIDGET_H
